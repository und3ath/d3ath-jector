﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Mono;
using Mono.Cecil;
using Mono.Cecil.Cil;
using System.Collections;
using System.Reflection;

namespace und3ath_Injector
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public  AssemblyDefinition LoadedAssembly { get; set; }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Executable | *.exe";
            if (open.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = open.FileName;
                looad();
            }
        }

        private void looad()
        {

            ImageList img = new ImageList();
           // img.Images.Add(Properties.Resources.imageList1_ImageStream7);
          //  img.Images.Add(Properties.Resources.imageList1_ImageStream74);
          //  img.Images.Add(Properties.Resources.imageList1_ImageStream49);
          //  img.Images.Add(Properties.Resources.verifier_vert_ok_oui_icone_7022_16);
            treeView1.ImageList = img;
            treeView1.SelectedImageIndex = 3;
            try
            {
                LoadedAssembly = AssemblyDefinition.ReadAssembly(textBox1.Text);
            }
            catch
            {
                MessageBox.Show("Couldnt Read Assembly, probably non .net file of unreadable obfuscation");
                return;
            }
            TreeNode noeud;
            IEnumerator enumerator = LoadedAssembly.MainModule.Types.GetEnumerator();
            while (enumerator.MoveNext())
            {
                TypeDefinition typeDefinition = (TypeDefinition)enumerator.Current;
                noeud = this.treeView1.Nodes.Add(typeDefinition.Name.ToString());
                noeud.ImageIndex = 0;
                IEnumerator enumerator2 = typeDefinition.Methods.GetEnumerator();
                while (enumerator2.MoveNext())
                {

                    MethodDefinition methodDefinition = (MethodDefinition)enumerator2.Current;
                    if (methodDefinition.IsConstructor)
                    {
                        noeud.Nodes.Add(methodDefinition.Name.ToString()).ImageIndex = 2;
                    }
                    noeud.Nodes.Add(methodDefinition.Name.ToString()).ImageIndex = 1;
                }
            }
            richTextBox1.Clear();
            richTextBox1.AppendText("Assembly Loaded." + Environment.NewLine);
            richTextBox1.AppendText(LoadedAssembly.MainModule.Runtime.ToString() + "  dependant." + Environment.NewLine);          
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            var assembly = AssemblyDefinition.ReadAssembly(textBox1.Text);
            try
            {
                IEnumerator enumerator = assembly.MainModule.Types.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    TypeDefinition typeDefinition = (TypeDefinition)enumerator.Current;

                    if (typeDefinition.Name == treeView1.SelectedNode.Parent.Text)
                    {
                        IEnumerator enumerator2 = typeDefinition.Methods.GetEnumerator();
                        while (enumerator2.MoveNext())
                        {
                            MethodDefinition methodDefinition = (MethodDefinition)enumerator2.Current;
                            if (methodDefinition.Name == treeView1.SelectedNode.Text && !methodDefinition.IsSetter && !methodDefinition.IsGetter)
                            {
                                richTextBox.Clear();
                                ILProcessor cilWorker = methodDefinition.Body.GetILProcessor();
                                foreach (Instruction ins in cilWorker.Body.Instructions)
                                {
                                    richTextBox.AppendText(ins + Environment.NewLine);
                                }
                            }
                        }
                    }
                }
            }
            catch
            {

            }
        }

        private void treeView1_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            Payloader pl = new Payloader(this);
            pl.Show();
        }

     
    }
}
